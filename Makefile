all:
ifndef VERSION
	$(error VERSION is undefined)
endif
	docker buildx build --platform linux/amd64 --build-arg website_version=${VERSION} -t website .
	docker tag website daisukixci/website
	docker tag website daisukixci/website:${VERSION}
	docker push daisukixci/website
	docker push daisukixci/website:${VERSION}
