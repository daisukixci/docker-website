FROM alpine:3.16 AS git

ARG website_version="0.1.0"

RUN apk update && apk add git
RUN git clone --single-branch --branch v${website_version} https://gitlab.com/daisukixci/website.git

FROM python:3.10-alpine

ENV HOME=/home/app
ENV APP_HOME=/home/app/website

RUN apk add --no-cache build-base linux-headers pcre-dev

RUN addgroup --system app && adduser --system app

COPY --from=git /website $APP_HOME
COPY uwsgi.ini $APP_HOME
RUN chown -R app:app $APP_HOME
WORKDIR $APP_HOME
USER app
ENV PATH="${HOME}/.local/bin/:${PATH}"
RUN python -m pip install --upgrade pip uwsgi
RUN python -m pip install .


CMD ["uwsgi", "--ini", "/home/app/website/uwsgi.ini"]
